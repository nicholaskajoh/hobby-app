var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var hobbySchema = new Schema({
  hobby: {type: String, required: true},
  userId: {type: Schema.Types.ObjectId, ref: 'User'},
  createdAt: Date,
  updatedAt: Date
});

hobbySchema.pre('save', async function(next) {
  // set dates
  var currentDate = new Date();
  if (!this.createdAt) this.createdAt = currentDate;
  this.updatedAt = currentDate;

  next();
});

var Hobby = mongoose.model('Hobby', hobbySchema);
module.exports = Hobby;