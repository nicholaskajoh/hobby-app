var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var Schema = mongoose.Schema;

var userSchema = new Schema({
  name: {
    type: String,
    required: [true, "A name is required"]
  },
  email: {
    type: String,
    required: [true, "An email is required"],
    unique: [true, "An account with this email already exists"]
  },
  phone: {
    type: String,
    required: [true, "An phone number is required"],
    unique: [true, "An account with this phone number already exists"]
  },
  password: {
    type: String,
    required: [true, "A password is required"]
  },
  createdAt: Date,
  updatedAt: Date
});

userSchema.pre('save', async function(next) {
  var user = this;

  // set dates
  var currentDate = new Date();
  if (!this.createdAt) this.createdAt = currentDate;
  this.updatedAt = currentDate;

  // hash password
  if (user.isModified('password')) {
    var salt = await bcrypt.genSalt(10);
    var hash = await bcrypt.hash(user.password, salt);
    user.password = hash;
  }

  next();
});

var User = mongoose.model('User', userSchema);
module.exports = User;