var express = require('express');
var router = express.Router();
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var User = require('../models/user');

router.post('/login', async (req, res, next) => {
  var email = req.body.email;
  var password = req.body.password;

  if (!email || !password) {
    var err = new Error('Email and password is required!');
    err.status = 400;
    next(err);
  }

  try {
    var user = await User.findOne({email});

    // 404
    if (!user) {
      var err = new Error('User not found!');
      err.status = 404;
      next(err);
    }

    // authenticate
    if (bcrypt.compareSync(password, user.password)) {
      const payload = {
        _id: user._id,
        name: user.name,
        email: user.email,
        phone: user.phone
      };
      var token = jwt.sign(payload, process.env.JWT_SECRET, {expiresIn: "7d"});
      res.json({token});
    } else {
      var err = new Error('Invalid username or password!');
      err.status = 401;
      next(err);
    }
  } catch (err) {
    next(err);
  }
});

router.post('/register', async (req, res, next) => {
  try {
    // create user
    var user = User({
      name: req.body.name,
      email: req.body.email,
      phone: req.body.phone,
      password: req.body.password
    });
    
    await user.save();
    res.json({message: "Registration successful!"});
  } catch (err) {
    next(err);
  }
});

module.exports = router;
