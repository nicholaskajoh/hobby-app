var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var authMiddleware = require("../middlewares/auth");
var Hobby = require("../models/hobby");

router.get("/hobbies", authMiddleware, async (req, res, next) => {
  try {
    hobbies = await Hobby.find({
      userId: mongoose.Types.ObjectId(req.user._id)
    })
      .sort("-createdAt")
      .exec();

    res.json(hobbies);
  } catch (err) {
    next(err);
  }
});

router.post("/add", authMiddleware, async (req, res, next) => {
  try {
    // create hobby
    var hobby = Hobby({
      hobby: req.body.hobby,
      userId: mongoose.Types.ObjectId(req.user._id)
    });

    await hobby.save();

    // send email via Mailgun
    var mailgun = require('mailgun-js')({
      apiKey: process.env.MAILGUN_API_KEY,
      domain: process.env.MAILGUN_DOMAIN
    });

    var data = {
      from: 'HobbyApp Team <enjoyment-officers@hobby-app.fun>',
      to: req.user.email,
      subject: 'New hobby',
      text: `Hi ${req.user.name}, you just added '${hobby.hobby}' to your hobbies!`
    };
    await mailgun.messages().send(data);

    // send SMS via Twilio
    var twilio = require('twilio')(
      process.env.TWILIO_ACCOUNT_SID,
      process.env.TWILIO_AUTH_TOKEN
    );

    try {
      await twilio.messages.create({
        from: process.env.TWILIO_PHONE_NUMBER,
        to: req.user.phone,
        body: `Hi ${req.user.name}, you just added '${hobby.hobby}' to your hobbies!`
      });
    } catch(error) {
      console.error(error);
    }

    res.json(hobby);
  } catch (err) {
    error.status = 400;
    next(err);
  }
});

router.delete("/delete/:id", authMiddleware, (req, res, next) => {
  Hobby.find({ _id: mongoose.Types.ObjectId(req.params.id) })
    .remove()
    .exec();

  res.json({});
});

module.exports = router;
