var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');
var mongoose = require('mongoose');

require('dotenv').config();

// db connection
mongoose.connect(process.env.MONGODB_URI);

// bind connection to error event
var db = mongoose.connection;
db.on('error', console.error.bind(
  console, 'MongoDB connection error:'
));

var app = express();

// configs
app.use(logger('dev'));
app.use(bodyParser.json({type: 'application/json'}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

// serve angular app
app.use('/', express.static(__dirname + "/dist/"));

// api endpoints
app.use('/api', require('./controllers/auth'));
app.use('/api', require('./controllers/hobby'));

// catch 404 and forward to error handler
app.use((req, res, next) => {
  var err = new Error('Not found!');
  err.status = 404;
  next(err);
});

// error handler
app.use((err, req, res, next) => {
  var status = err.status || 500;
  var message = err.message || "Server error!";

  res.status(status).json({
    status, message
  });
});

module.exports = app;
