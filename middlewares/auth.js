var jwt = require('jsonwebtoken');

var auth = (req, res, next) => {
  var token = req.body.token || req.query.token || req.headers['x-access-token'];

  if (token) {
    try {
      var decoded = jwt.verify(token, process.env.JWT_SECRET);
      req.user = decoded;
      next();
    } catch(err) {
      err.status = 401;
      err.message = "Invalid access token!";
      next(err);
    }
  } else {
    var err = new Error('Not auth token provided!');
    err.status = 403;
    next(err);
  }
};

module.exports = auth;