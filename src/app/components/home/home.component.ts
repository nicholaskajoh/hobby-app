import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { HobbyService } from '../../services/hobby/hobby.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    public authService: AuthService,
    public hobbyService: HobbyService
  ) {
    this.authService.checkAuth();
  }

  ngOnInit() {
    this.hobbyService.getHobbies();
  }
}
