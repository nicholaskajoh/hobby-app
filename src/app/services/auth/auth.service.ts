import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/finally';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  loginEmail: string;
  loginPassword: string;
  registerName: string;
  registerEmail: string;
  registerPhone: string;
  registerPassword: string;
  accessToken: string = null;
  isLoggingIn: boolean = false;
  hasLoginErrors: boolean = false;
  loginErrorMessage: string;
  isRegistering: boolean = false;
  hasRegistrationErrors: boolean = false;
  registerErrorMessage: string;
  hasRegistered: boolean = false;

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  isAuth() {
    if (this.accessToken != null)
      return true;
    return false;
  }

  checkAuth() {
    if (!this.isAuth()) {
      this.router.navigateByUrl('/auth');
      alert("You're not logged in!");
    }
  }

  login() {
    var email = this.loginEmail;
    var password = this.loginPassword;
    this.isLoggingIn = true;

    var onNext = (data) => {
      this.accessToken = data.token;
      this.router.navigateByUrl('/');
    };

    var onError = (data) => {
      this.hasLoginErrors = true;
      this.loginErrorMessage = data.error.message;
    };

    var onComplete = () => {
      this.isLoggingIn = false;
    }

    this.http
      .post(`${environment.apiUrl}/api/login`, {email, password})
      .finally(onComplete)
      .subscribe(onNext, onError);
  }

  register () {
    var name = this.registerName;
    var email = this.registerEmail;
    var phone = this.registerPhone;
    var password = this.registerPassword;
    this.hasRegistered = false;
    this.isRegistering = true;

    var onNext = (data) => {
      this.hasRegistered = true;
      this.hasRegistrationErrors = false;
      this.registerName = "";
      this.registerEmail = "";
      this.registerPhone = "";
      this.registerPassword = "";
    };

    var onError = (data) => {
      this.hasRegistrationErrors = true;
      this.registerErrorMessage = data.error.message;
    };

    var onComplete = () => {
      this.isRegistering = false;
    }

    this.http
      .post(`${environment.apiUrl}/api/register`, {name, email, phone, password})
      .finally(onComplete)
      .subscribe(onNext, onError);
  }

  logout() {
    this.accessToken = null;
    this.router.navigateByUrl('/auth');
  }
}
