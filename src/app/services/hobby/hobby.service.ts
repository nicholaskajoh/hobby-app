import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class HobbyService {
  hobbies: Array<Object> = [];
  newHobby: String;
  isAddingHobby: boolean = false;

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) { }

  getHobbies() {
    var onNext = (data) => {
      this.hobbies = data;
    };

    var onError = (data) => { };

    var onComplete = () => { }

    this.http
      .get(`${environment.apiUrl}/api/hobbies`, {
        headers: {'x-access-token': this.authService.accessToken}
      })
      .finally(onComplete)
      .subscribe(onNext, onError);
  }

  addHobby() {
    this.isAddingHobby = true;

    var onNext = (data) => {
      this.newHobby = "";
      this.getHobbies();
    };

    var onError = (data) => {
      console.log(data.error.message);
    };

    var onComplete = () => {
      this.isAddingHobby = false;
    }

    this.http
      .post(`${environment.apiUrl}/api/add`, {
        hobby: this.newHobby
      }, {
        headers: {'x-access-token': this.authService.accessToken}
      })
      .finally(onComplete)
      .subscribe(onNext, onError);
  }

  deleteHobby(id: string) {
    var onNext = (data) => {
      this.getHobbies();
    };

    var onError = (data) => {
      console.log(data.error.message);
    };

    var onComplete = () => { }

    this.http
      .delete(`${environment.apiUrl}/api/delete/${id}`, {
        headers: {'x-access-token': this.authService.accessToken}
      })
      .finally(onComplete)
      .subscribe(onNext, onError);
  }
}
